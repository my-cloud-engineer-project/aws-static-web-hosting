# aws-static-web-hosting

## Static Website hosting in S3

- create index.html and error.html pages

- create s3 bucket

```
aws s3api create-bucket --bucket "your bucket name" --region "region e.g. us-east-1"
```

- upload files to s3
```
 aws s3 cp index.html s3://fortune.zaratedev.org/index.html
```

- set file to public 
** you can do this in the console OR
```
aws s3api put-object-acl --bucket awsExampleBucket --key exampleObject --acl public-read
```


- check to see if you can return index.html in your browser via url

picture for proof

![alt text](screenshot.png?raw=true "screenshot.png")


## Bonus steps

- if you have a domain repeate the above steps

- create a bucket that has "fortune.yourdomainname.com"

- upload files and make them public

- create an alias record name and set the bucket url to point to "fortune.yourdomain.com" Hint * this will point all incoming traffic that gets resolved by the DNS in route53 to point traffic to your bucket*